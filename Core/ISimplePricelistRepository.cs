using PricelistService.Core.Models;
using System.Collections.Generic;

namespace PricelistService.Core
{
    public interface ISimplePricelistRepository
    {
        SimplePricelist Get();
        IList<PricelistEntry> GetEntries(int categoryId);
        PricelistEntry GetEntry(int categoryId, int entryId);
        void AddEntry(int categoryId, PricelistEntry entry);
        void UpdateEntry(int categoryId, int entryId, PricelistEntry entry);
        void DeleteEntry(int categoryId, int entryId);
        IList<PricelistCategory> GetCategories();
        PricelistCategory GetCategory(int categoryId);
        void AddCategory(PricelistCategory category);
        void UpdateCategory(int categoryId, PricelistCategory category);
        void DeleteCategory(int categoryId);
    }
}