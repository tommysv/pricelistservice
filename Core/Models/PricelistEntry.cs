﻿using Newtonsoft.Json;

namespace PricelistService.Core.Models
{
    public class PricelistEntry
    {
        [JsonProperty(Order = 1)]
        public int Id { get; set; }

        [JsonProperty(Order = 2)]
        public string Name { get; set; }

        [JsonProperty(Order = 3)]
        public double Price { get; set; }

        [JsonProperty(Order = 4)]
        public int Order { get; set; }
    }
}