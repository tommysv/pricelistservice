﻿using Newtonsoft.Json;
using System;

namespace PricelistService.Core.Models
{
    public abstract class Pricelist
    {
        [JsonProperty(Order = 1)]
        public string Name { get; set; }

        [JsonProperty(Order = 2)]
        public string Description { get; set; }

        [JsonProperty(Order = 3)]
        public DateTime LastUpdated { get; set; }
    }
}