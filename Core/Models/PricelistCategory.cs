﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace PricelistService.Core.Models
{
    public class PricelistCategory
    {
        [JsonProperty(Order = 1)]
        public int Id { get; set; }

        [JsonProperty(Order = 2)]
        public string Name { get; set; }

        [JsonProperty(Order = 3)]
        public int Order { get; set; }

        [JsonProperty(Order = 4)]
        public IList<PricelistEntry> Entries { get; set; }
    }
}