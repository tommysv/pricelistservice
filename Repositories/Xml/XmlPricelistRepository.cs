﻿using PricelistService.Core;
using PricelistService.Core.Models;
using System.Collections.Generic;

namespace PricelistService.Repositories.Xml
{
    public class XmlPricelistRepository : ISimplePricelistRepository
    {
        public SimplePricelist Get()
        {
            throw new System.NotImplementedException();
        }

        public IList<PricelistEntry> GetEntries(int categoryId)
        {
            throw new System.NotImplementedException();
        }

        public PricelistEntry GetEntry(int categoryId, int entryId)
        {
            throw new System.NotImplementedException();
        }

        public void AddEntry(int categoryId, PricelistEntry entry)
        {
            throw new System.NotImplementedException();
        }

        public void UpdateEntry(int categoryId, int entryId, PricelistEntry entry)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteEntry(int categoryId, int entryId)
        {
            throw new System.NotImplementedException();
        }

        public IList<PricelistCategory> GetCategories()
        {
            throw new System.NotImplementedException();
        }

        public PricelistCategory GetCategory(int categoryId)
        {
            throw new System.NotImplementedException();
        }

        public void AddCategory(PricelistCategory category)
        {
            throw new System.NotImplementedException();
        }

        public void UpdateCategory(int categoryId, PricelistCategory category)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteCategory(int categoryId)
        {
            throw new System.NotImplementedException();
        }
    }
}