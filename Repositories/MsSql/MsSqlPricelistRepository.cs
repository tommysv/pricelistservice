﻿using PricelistService.Core;
using PricelistService.Core.Models;
using System;
using System.Collections.Generic;

namespace PricelistService.Repositories.MsSql
{
    public class MsSqlPricelistRepository : ISimplePricelistRepository
    {
        public SimplePricelist Get()
        {
            throw new NotImplementedException();
        }

        public IList<PricelistEntry> GetEntries(int categoryId)
        {
            throw new NotImplementedException();
        }

        public PricelistEntry GetEntry(int categoryId, int entryId)
        {
            throw new NotImplementedException();
        }

        public void AddEntry(int categoryId, PricelistEntry entry)
        {
            throw new NotImplementedException();
        }

        public void UpdateEntry(int categoryId, int entryId, PricelistEntry entry)
        {
            throw new NotImplementedException();
        }

        public void DeleteEntry(int categoryId, int entryId)
        {
            throw new NotImplementedException();
        }

        public IList<PricelistCategory> GetCategories()
        {
            throw new NotImplementedException();
        }

        public PricelistCategory GetCategory(int categoryId)
        {
            throw new NotImplementedException();
        }

        public void AddCategory(PricelistCategory category)
        {
            throw new NotImplementedException();
        }

        public void UpdateCategory(int categoryId, PricelistCategory category)
        {
            throw new NotImplementedException();
        }

        public void DeleteCategory(int categoryId)
        {
            throw new NotImplementedException();
        }
    }
}