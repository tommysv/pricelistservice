﻿using PricelistService.Core;
using PricelistService.Core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace PricelistService.Repositories.InMemory
{
    public class InMemoryPricelistRepository : HttpContextBase, ISimplePricelistRepository
    {
        public SimplePricelist Get()
        {
            return (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];
        }

        public IList<PricelistEntry> GetEntries(int categoryId)
        {
            var simplePricelist = (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];
            var pricelistCategory = simplePricelist.Categories.SingleOrDefault(c => c.Id == categoryId);
            if (pricelistCategory == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return pricelistCategory.Entries;
        }

        public PricelistEntry GetEntry(int categoryId, int entryId)
        {
            var simplePricelist = (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];
            var pricelistCategory = simplePricelist.Categories.SingleOrDefault(c => c.Id == categoryId);
            if (pricelistCategory == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            var pricelistEntry = pricelistCategory.Entries.SingleOrDefault(e => e.Id == entryId);
            if (pricelistEntry == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return pricelistEntry;
        }

        public void AddEntry(int categoryId, PricelistEntry entry)
        {
            var simplePricelist = (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];
            var pricelistCategory = simplePricelist.Categories.SingleOrDefault(c => c.Id == categoryId);
            if (pricelistCategory == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var nextId = pricelistCategory.Entries.Max(e => e.Id) + 1;
            var nextOrder = pricelistCategory.Entries.Max(e => e.Order) + 1;

            var pricelistEntry = new PricelistEntry
            {
                Id = nextId,
                Name = entry.Name,
                Price = entry.Price,
                Order = nextOrder
            };
            pricelistCategory.Entries.Add(pricelistEntry);
        }

        public void UpdateEntry(int categoryId, int entryId, PricelistEntry entry)
        {
            var simplePricelist = (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];
            var pricelistCategory = simplePricelist.Categories.SingleOrDefault(c => c.Id == categoryId);
            if (pricelistCategory == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var pricelistEntry = pricelistCategory.Entries.SingleOrDefault(e => e.Id == entryId);
            if (pricelistEntry == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            pricelistEntry.Name = entry.Name;
            pricelistEntry.Price = entry.Price;
            pricelistEntry.Order = entry.Order;
        }

        public void DeleteEntry(int categoryId, int entryId)
        {
            var simplePricelist = (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];
            var pricelistCategory = simplePricelist.Categories.SingleOrDefault(c => c.Id == categoryId);
            if (pricelistCategory == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            var pricelistEntry = pricelistCategory.Entries.SingleOrDefault(e => e.Id == entryId);
            if (pricelistEntry == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            pricelistCategory.Entries.Remove(pricelistEntry);
        }

        public IList<PricelistCategory> GetCategories()
        {
            var simplePricelist = (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];
            return simplePricelist.Categories;
        }

        public PricelistCategory GetCategory(int categoryId)
        {
            var simplePricelist = (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];
            var pricelistCategory = simplePricelist.Categories.SingleOrDefault(c => c.Id == categoryId);
            if (pricelistCategory == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            return pricelistCategory;
        }

        public void AddCategory(PricelistCategory category)
        {
            var simplePricelist = (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];

            var nextId = simplePricelist.Categories.Max(c => c.Id) + 1;
            var nextOrder = simplePricelist.Categories.Max(c => c.Order) + 1;

            var pricelistCategory = new PricelistCategory
            {
                Id = nextId,
                Name = category.Name,
                Order = nextOrder
            };
            simplePricelist.Categories.Add(pricelistCategory);
        }

        public void UpdateCategory(int categoryId, PricelistCategory category)
        {
            var simplePricelist = (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];
            var pricelistCategory = simplePricelist.Categories.SingleOrDefault(c => c.Id == categoryId);
            if (pricelistCategory == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);

            pricelistCategory.Name = category.Name;
            pricelistCategory.Order = category.Order;
            pricelistCategory.Entries = category.Entries;
        }

        public void DeleteCategory(int categoryId)
        {
            var simplePricelist = (SimplePricelist)HttpContext.Current.Application["InMemoryPricelist"];
            var pricelistCategory = simplePricelist.Categories.SingleOrDefault(c => c.Id == categoryId);
            if (pricelistCategory == null)
                throw new HttpResponseException(HttpStatusCode.NotFound);
            simplePricelist.Categories.Remove(pricelistCategory);
        }
    }
}