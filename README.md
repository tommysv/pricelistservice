ASP .NET Web Api (RESTful) for managing a simple in-memory pricelist (for demonstrational purposes).
Data is output in Json-format og input is specified in Json-format.

Supported HTTP verbs (operations):
GET, POST, PUT and DELETE.

PricelistService - Examples

/api/pricelist

	Get entire pricelist
	
/api/pricelist/categories

	GET a list of categories or POST a new category
	
/api/pricelist/categories/1

	GET/PUT/DELETE a single category
	
/api/pricelist/categories/1/entries

	GET a list of entries in category or POST a new entry in category
	
/api/pricelist/categories/1/entries/1

	GET/PUT/DELETE a specific entry in category



