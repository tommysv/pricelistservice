using PricelistService.Core;
using PricelistService.Core.Models;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PricelistService.Controllers.api
{
    public class SimplePricelistCategoriesController : ApiController
    {
        private readonly ISimplePricelistRepository _simplePricelistRepository;

        public SimplePricelistCategoriesController(ISimplePricelistRepository simplePricelistRepository)
        {
            _simplePricelistRepository = simplePricelistRepository;
        }

        public HttpResponseMessage Get()
        {
            HttpResponseMessage response;
            try
            {
                var simplePricelistCategories = _simplePricelistRepository.GetCategories();
                response = Request.CreateResponse(HttpStatusCode.OK, simplePricelistCategories);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return response;
        }

        public HttpResponseMessage Post([FromBody]PricelistCategory category)
        {
            try
            {
                _simplePricelistRepository.AddCategory(category);
            }
            catch (HttpResponseException e)
            {
                return Request.CreateErrorResponse(e.Response.StatusCode, e.Response.ReasonPhrase);
            }
            catch (System.Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error");
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}