﻿using Newtonsoft.Json.Serialization;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace PricelistService
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            config.Routes.MapHttpRoute(
                name: "SimplePricelistEntry",
                routeTemplate: "api/pricelist/categories/{categoryId}/entries/{entryId}",
                defaults: new { controller = "simplepricelistentry" }
                );

            config.Routes.MapHttpRoute(
                name: "SimplePricelistEntries",
                routeTemplate: "api/pricelist/categories/{categoryId}/entries",
                defaults: new { controller = "simplepricelistentries" }
                );

            config.Routes.MapHttpRoute(
                name: "SimplePricelistCategory",
                routeTemplate: "api/pricelist/categories/{categoryId}",
                defaults: new { controller = "simplepricelistcategory" }
                );

            config.Routes.MapHttpRoute(
                name: "SimplePricelistCategories",
                routeTemplate: "api/pricelist/categories",
                defaults: new { controller = "simplepricelistcategories" }
                );

            config.Routes.MapHttpRoute(
                name: "SimplePricelist",
                routeTemplate: "api/pricelist/",
                defaults: new { controller = "simplepricelist" }
                );

            config.Routes.MapHttpRoute(
                name: "ResourceNotFound",
                routeTemplate: "api/{*uri}",
                defaults: new { controller = "Default", uri = RouteParameter.Optional });

            config.Formatters.Clear();
            config.Formatters.Add(new JsonMediaTypeFormatter());
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            config.Formatters.JsonFormatter.UseDataContractJsonSerializer = false;
        }
    }
}
